from django.contrib import admin

from uniwersity.models import (
    Discipline,
    Lecture,
    Laboratory,
    Practical,
    Faculty,
    Specialty,
    Course,
    Questions,
    Choice,
    GradeAssignment,
    Schedule,
    TimeTable,
)


class DisciplineAdmin(admin.ModelAdmin):
    list_display = ["id", "name", "teacher"]


class LectureAdmin(admin.ModelAdmin):
    list_display = ["id", "title", "lecture_file", "discipline"]


class LaboratoryAdmin(admin.ModelAdmin):
    list_display = ["id", "title", "laboratory_file", "discipline"]


class PracticalAdmin(admin.ModelAdmin):
    list_display = ["id", "title", "practical_file", "discipline"]


admin.site.register(Discipline, DisciplineAdmin)
admin.site.register(Lecture, LectureAdmin)
admin.site.register(Laboratory, LaboratoryAdmin)
admin.site.register(Practical, PracticalAdmin)
admin.site.register(Faculty)
admin.site.register(Specialty)
admin.site.register(Course)
admin.site.register(Questions)
admin.site.register(Choice)
admin.site.register(GradeAssignment)
admin.site.register(Schedule)
admin.site.register(TimeTable)
