from django.apps import AppConfig


class UniwersityConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "uniwersity"
