from django.db import models


def lecture_directory_path(instance, filename):
    discipline = instance.discipline.name
    return f"files/{discipline}/lecture/{filename}"


def laboratory_directory_path(instance, filename):
    discipline = instance.discipline.name
    return f"files/{discipline}/laboratory/{filename}"


def practical_directory_path(instance, filename):
    discipline = instance.discipline.name
    return f"files/{discipline}/practical/{filename}"


class Faculty(models.Model):
    name = models.CharField(max_length=120)

    class Meta:
        db_table = "faculty"

    def __str__(self):
        return f"{self.name}"


class Course(models.Model):
    name = models.IntegerField()

    class Meta:
        db_table = "course"

    def __str__(self):
        return f"{self.name}"


class Specialty(models.Model):
    name = models.CharField(max_length=120)

    class Meta:
        db_table = "specialt"

    def __str__(self):
        return f"{self.name}"


class Discipline(models.Model):
    name = models.CharField(max_length=120)
    teacher = models.ForeignKey(
        "user.UserProfile",
        on_delete=models.CASCADE,
        related_name="discipline",
        null=True,
    )

    class Meta:
        db_table = "discipline"

    def __str__(self):
        return f"{self.name}"


class Lecture(models.Model):
    title = models.CharField(max_length=120)
    lecture_file = models.FileField(upload_to=lecture_directory_path)
    discipline = models.ForeignKey(
        Discipline, on_delete=models.CASCADE, related_name="lecture"
    )

    class Meta:
        db_table = "lecture"

    def __str__(self):
        return f"{self.title}"


class Laboratory(models.Model):
    title = models.CharField(max_length=120)
    laboratory_file = models.FileField(upload_to=laboratory_directory_path)
    discipline = models.ForeignKey(
        Discipline, on_delete=models.CASCADE, related_name="laboratory"
    )

    class Meta:
        db_table = "laboratory"

    def __str__(self):
        return f"{self.title}"


class Practical(models.Model):
    title = models.CharField(max_length=120)
    practical_file = models.FileField(upload_to=practical_directory_path)
    discipline = models.ForeignKey(
        Discipline, on_delete=models.CASCADE, related_name="practical"
    )

    class Meta:
        db_table = "practical"

    def __str__(self):
        return f"{self.title}"


# Create test for student
class GradeAssignment(models.Model):
    student = models.ForeignKey(
        "user.UserProfile", on_delete=models.CASCADE, related_name="student"
    )
    assignment = models.ForeignKey(
        Discipline,
        on_delete=models.SET_NULL,
        related_name="discipline",
        blank=True,
        null=True,
    )
    grade = models.FloatField()

    def __str__(self):
        return self.student.username

    class Meta:
        db_table = "grade_assignment"


class Choice(models.Model):
    title = models.CharField(max_length=50)

    def __str__(self):
        return self.title

    class Meta:
        db_table = "choice"


class Questions(models.Model):
    question = models.CharField(max_length=200)
    choices = models.ManyToManyField(Choice)
    answer = models.ForeignKey(
        Choice, on_delete=models.CASCADE, related_name="answer", blank=True, null=True
    )
    assignment = models.ForeignKey(
        Discipline,
        on_delete=models.CASCADE,
        related_name="assignment",
        blank=True,
        null=True,
    )

    def __str__(self) -> str:
        return self.question

    class Meta:
        db_table = "questions"


# Create schedules
class TimeTable(models.Model):
    DAYS = (
        ("1", "Monday"),
        ("2", "Tuesday"),
        ("3", "Wednesday"),
        ("4", "Thursday"),
        ("5", "Friday"),
        ("6", "Saturday"),
        ("7", "Sunday"),
    )

    day = models.CharField(max_length=10, choices=DAYS)


class Schedule(models.Model):
    name = models.CharField(max_length=100, default="Schedule")
    faculty = models.ForeignKey(Faculty, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    specialty = models.ForeignKey(Specialty, on_delete=models.CASCADE)
    discipline = models.ForeignKey(Discipline, on_delete=models.CASCADE)
    time_start = models.TimeField()
    time_end = models.TimeField()
    day_table = models.ForeignKey(TimeTable, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return (
            f"Discipline: {self.discipline}, Time: {self.time_start} - {self.time_end}"
        )

    class Meta:
        db_table = "schedule"
