from rest_framework import serializers


from django.core.validators import FileExtensionValidator

from rest_framework.exceptions import APIException

from uniwersity.models import (
    Faculty,
    Course,
    Specialty,
    Discipline,
    Lecture,
    Laboratory,
    Practical,
    Questions,
    Choice,
    Schedule,
    TimeTable,
)


class FacultyUpSerializer(serializers.ModelSerializer):
    """Implementation of the faculty"""

    class Meta:
        model = Faculty
        fields = ("name",)

    def create(self, validated_data):
        faculty = Faculty.objects.create(
            name=validated_data.get("name"),
        )

        return faculty


class FacultytListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Faculty
        fields = ("name",)


class CourseUpSerializer(serializers.ModelSerializer):
    """Implementation of the course"""

    class Meta:
        model = Course
        fields = ("name",)

    def create(self, validated_data):
        course = Course.objects.create(
            name=validated_data.get("name"),
        )

        return course


class CourseListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Faculty
        fields = ("name",)


class SpecialtyUpSerializer(serializers.ModelSerializer):
    """Implementation of specialty"""

    class Meta:
        model = Specialty
        fields = ("name",)

    def create(self, validated_data):
        specialty = Specialty.objects.create(
            name=validated_data.get("name"),
        )

        return specialty


class SpecialtyListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Faculty
        fields = ("name",)


# Serializers discipline
class DisciplineUpSerializer(serializers.ModelSerializer):
    """Implementation of specialty"""

    class Meta:
        model = Discipline
        fields = ("name",)

    def create(self, validated_data):
        discipline = Discipline.objects.create(
            name=validated_data.get("name"),
        )

        return discipline


class DisciplineListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Discipline
        fields = ("name",)


# Serializers Lecture
class LectureUpSerializer(serializers.ModelSerializer):
    """Implementation of specialty"""

    lecture_file = serializers.FileField(validators=[FileExtensionValidator(["txt"])])

    class Meta:
        model = Lecture
        fields = "__all__"


class LectureListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lecture
        fields = "__all__"


# Serializers Laboratory
class LaboratoryUpSerializer(serializers.ModelSerializer):
    """Implementation of specialty"""

    laboratory_file = serializers.FileField(
        validators=[FileExtensionValidator(["txt"])]
    )

    class Meta:
        model = Laboratory
        fields = "__all__"


class LaboratoryListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Laboratory
        fields = "__all__"


# Serializers Practical
class PracticalUpSerializer(serializers.ModelSerializer):
    """Implementation of specialty"""

    practical_file = serializers.FileField(validators=[FileExtensionValidator(["txt"])])

    class Meta:
        model = Practical
        fields = "__all__"


class PracticalListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Practical
        fields = "__all__"


# Serializers Task
class StringSerializers(serializers.StringRelatedField):
    def to_internal_value(self, value):
        return value


class QuestionsSerializer(serializers.ModelSerializer):
    choices = StringSerializers(many=True)
    answer = serializers.CharField()

    class Meta:
        model = Questions
        fields = ("choices", "question", "answer")


class AssignmentSerializer(serializers.Serializer):
    questions = QuestionsSerializer(many=True)
    discipline = serializers.IntegerField()

    class Meta:
        fields = ("questions", "discipline")

    def create(self, request):
        data = request.data

        question_item = Questions()
        assignments = Discipline.objects.get(id=data["discipline"])
        question_item.assignment = assignments

        for item in data["questions"]:
            if item["answer"] not in item["choices"]:
                raise APIException("Not answer in choices")
            question_item.question = item.get("question")
            question_item.save()

            for chois in item["choices"]:
                new_choice = Choice()
                new_choice.title = chois
                new_choice.save()
                question_item.choices.add(new_choice)

            question_item.answer = Choice.objects.get(title=item["answer"])
            question_item.save()
        return question_item


class QuestionListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Questions
        fields = "__all__"


# Schedule Serializer
class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = (
            "faculty",
            "course",
            "specialty",
            "discipline",
            "time_start",
            "time_end",
            "day_table",
        )


class ScheduleListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = "__all__"


class DataListSerializer(serializers.ModelSerializer):
    day = serializers.SerializerMethodField()

    class Meta:
        model = TimeTable
        fields = ("id", "day")

    def get_day(self, obj):
        return obj.get_day_display()
