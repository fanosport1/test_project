from typing import Union, List, Optional, Dict
from rest_framework.serializers import Serializer


def set_custom_data(
    status: bool = True,
    message: str = "",
    data: Optional[Union[Serializer, List]] = None,
) -> Dict[str, Union[bool, str, Union[Serializer, List]]]:
    custom_data = {
        "status": status,
        "message": message,
    }
    if data:
        custom_data["data"] = data
    return custom_data
