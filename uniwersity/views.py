# Create your views here.

from rest_framework import generics
from .permissions import IsStaff
from .models import (
    Faculty,
    Course,
    Specialty,
    Lecture,
    Laboratory,
    Practical,
    Questions,
    Schedule,
    TimeTable,
)

from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import FormParser, MultiPartParser

from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST

from .serializer import (
    FacultyUpSerializer,
    FacultytListSerializer,
    CourseUpSerializer,
    CourseListSerializer,
    SpecialtyUpSerializer,
    SpecialtyListSerializer,
    DisciplineUpSerializer,
    DisciplineListSerializer,
    LectureUpSerializer,
    LaboratoryUpSerializer,
    PracticalUpSerializer,
    LectureListSerializer,
    LaboratoryListSerializer,
    PracticalListSerializer,
    AssignmentSerializer,
    QuestionListSerializer,
    ScheduleSerializer,
    ScheduleListSerializer,
    DataListSerializer,
)
from .utils import set_custom_data


class FacultyCreate(generics.CreateAPIView):
    """
    post:
    Create new faculty
    Register a user with obtained params
    """

    permission_classes = (IsStaff,)
    serializer_class = FacultyUpSerializer


class FacultyView(generics.ListAPIView):
    # pagination_class = StudentPagination
    queryset = Faculty.objects.all()
    serializer_class = FacultytListSerializer


class CourseCreate(generics.CreateAPIView):
    """
    post:
    Create new course
    Register a user with obtained params
    """

    permission_classes = (IsStaff,)
    serializer_class = CourseUpSerializer


class CourseView(generics.ListAPIView):
    # pagination_class = StudentPagination
    queryset = Course.objects.all()
    serializer_class = CourseListSerializer


class SpecialtyCreate(generics.CreateAPIView):
    """
    post:
    Create new specialty
    Register a user with obtained params
    """

    permission_classes = (IsStaff,)
    serializer_class = SpecialtyUpSerializer


class SpecialtyView(generics.ListAPIView):
    # pagination_class = StudentPagination
    queryset = Specialty.objects.all()
    serializer_class = SpecialtyListSerializer


# Discipline create
class DisciplineCreate(generics.CreateAPIView):
    # permission_classes = (IsStaff, )
    serializer_class = DisciplineUpSerializer


class DisciplineView(generics.ListAPIView):
    # pagination_class = StudentPagination
    queryset = Specialty.objects.all()
    serializer_class = DisciplineListSerializer


class ProjectCreate(generics.CreateAPIView):
    action_serializers = {
        "/uniwersity/lecture-create/": LectureUpSerializer,
        "/uniwersity/laboratory-create/": LaboratoryUpSerializer,
        "/uniwersity/practical-create/": PracticalUpSerializer,
    }

    def get_serializer_class(self):
        return self.action_serializers[self.request.get_full_path()]

    def post(self, request):
        serializer = self.get_serializer_class()(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(
                set_custom_data(
                    message="Successfully uploaded your profile picture.",
                    data=serializer.data,
                ),
                status=status.HTTP_201_CREATED,
            )
        return Response(
            set_custom_data(status=False, message=serializer.errors),
            status=status.HTTP_200_OK,
        )


# Lecture create
class LectureCreate(generics.CreateAPIView):
    # permission_classes = (IsStaff, )
    parser_classes = (MultiPartParser, FormParser)
    serializer_class = LectureUpSerializer


class LectureView(generics.ListAPIView):
    # pagination_class = StudentPagination
    queryset = Lecture.objects.all()
    serializer_class = LectureListSerializer


# Laboratory create
class LaboratoryCreate(generics.CreateAPIView):
    # permission_classes = (IsStaff, )
    parser_classes = (MultiPartParser, FormParser)
    serializer_class = LaboratoryUpSerializer


class LaboratoryView(generics.ListAPIView):
    # pagination_class = StudentPagination
    queryset = Laboratory.objects.all()
    serializer_class = LaboratoryListSerializer


# Practical create
class PracticalCreate(generics.CreateAPIView):
    # permission_classes = (IsStaff, )
    parser_classes = (MultiPartParser, FormParser)
    serializer_class = PracticalUpSerializer


class PracticalView(generics.ListAPIView):
    # pagination_class = StudentPagination
    queryset = Practical.objects.all()
    serializer_class = PracticalListSerializer


# Task create
class AssignmentCreate(generics.CreateAPIView):
    serializer_class = AssignmentSerializer
    queryset = Questions.objects.all()

    def create(self, request):
        serializer = AssignmentSerializer(data=request.data)
        if serializer.is_valid():
            assignment = serializer.create(request)
            if assignment:
                return Response(status=HTTP_201_CREATED)
        return Response(status=HTTP_400_BAD_REQUEST)


class QuestionView(generics.ListAPIView):
    # pagination_class = StudentPagination
    queryset = Questions.objects.all()
    serializer_class = QuestionListSerializer


# Schedule create
class ScheduleCreate(generics.CreateAPIView):
    # permission_classes = (IsStaff, )

    serializer_class = ScheduleSerializer


class ScheduleView(generics.ListAPIView):
    # permission_classes = (IsStaff, )
    queryset = Schedule.objects.all()
    serializer_class = ScheduleListSerializer


class DataView(generics.ListAPIView):
    # permission_classes = (IsStaff, )
    queryset = TimeTable.objects.all()
    serializer_class = DataListSerializer
