from rest_framework import permissions

from user.models import UserProfile


class IsStaff(permissions.BasePermission):
    def has_permission(self, request, view):
        if (
            request.user.is_authenticated
            and request.user.userprofile.user_role == UserProfile.STUFF
        ):
            return True
