import asyncio

number_x: int = 0
number_y: int = 0


async def async_func() -> int:
    await asyncio.sleep(2)
    return number_y
