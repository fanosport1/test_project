# Student Project

## How to Run the Project Locally

### Setting Up the Environment

Open your terminal and follow these steps:

1. Create a virtual environment:
   ```bash
   python3 -m venv env_student
2. Install the required dependencies:
   ```bash
   pip3 install -r requirements.txt

### Docker Configuration
Make sure Docker is installed on your system. Then:
1. Build the Docker containers:
   ```bash
   docker-compose build
2. Start the Docker containers:
   ```bash
   docker-compose up

### Using Ruff for Code Quality Checks
For code quality checks using Ruff, use the following commands:
1. To check your code:
   ```bash
   ruff check .
2. If issues are detected, try fixing them automatically:
   ```bash
   ruff check --fix .
3. To format the code:
   ```bash
   ruff format .