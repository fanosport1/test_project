from rest_framework import generics
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from user.models import UserProfile
from user.paginators import UserPagination
from user.serializer import (
    SignUpSerializer,
    StudentListSerializer,
    TeacherListSerializer,
    StuffListSerializer,
    TestSerializer,
)
from uniwersity.permissions import IsStaff


class SignUpView(generics.CreateAPIView):
    """
    post:
    Create new user
    Register a user with obtained params
    """

    permission_classes = (AllowAny,)
    serializer_class = SignUpSerializer


class StudentsView(generics.ListAPIView):
    pagination_class = UserPagination
    queryset = UserProfile.objects.filter(user_role=UserProfile.STUDENT)
    permission_classes = (IsAuthenticated, IsStaff)
    serializer_class = StudentListSerializer


class TeachersView(generics.ListAPIView):
    pagination_class = UserPagination
    queryset = UserProfile.objects.filter(user_role=UserProfile.TEACHER)
    permission_classes = (IsAuthenticated,)
    serializer_class = TeacherListSerializer


class StuffView(generics.ListAPIView):
    pagination_class = UserPagination
    queryset = UserProfile.objects.filter(user_role=UserProfile.STUFF)
    permission_classes = (IsAuthenticated,)
    serializer_class = StuffListSerializer


class SignUpMultyUsers(generics.CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = TestSerializer

    def post(self, request):
        request_data = request.data
        for item in request_data["params"]:
            user = SignUpSerializer(data=item)
            if user.is_valid():
                user.save()
        return Response({"status: 201"})
