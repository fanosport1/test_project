from rest_framework import permissions
from models import UserProfile


class IsTeacher(permissions.BasePermission):
    def has_permission(self, request, view):
        if (
            request.user.is_authenticated
            and request.user.userprofile.user_role == UserProfile.TEACHER
        ):
            return True
