from django.db import models
from django.contrib.auth.models import User, UserManager
from django.utils import timezone
from uniwersity.models import Faculty, Course, Specialty


class UserProfile(User):
    # User role
    TEACHER = 0
    STUDENT = 1
    STUFF = 2

    USER_ROLES = (
        (TEACHER, "Teacher"),
        (STUDENT, "Student"),
        (STUFF, "Stuff"),
    )
    # Gender
    GENDER_CHOICES = ((0, "Male"), (1, "Female"))
    user_role = models.IntegerField(choices=USER_ROLES, default=STUDENT)
    gender = models.IntegerField(choices=GENDER_CHOICES, blank=True, null=True)
    born_date = models.DateTimeField(default=timezone.now)
    is_admin = models.BooleanField(default=False)
    faculty = models.ForeignKey(
        Faculty,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="faculty",
    )
    specialty = models.ForeignKey(
        Specialty,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="specialty",
    )
    course = models.ForeignKey(
        Course, on_delete=models.SET_NULL, blank=True, null=True, related_name="course"
    )
    objects = UserManager()

    class Meta:
        db_table = "user_profile"
