from django.urls import path

from user.views import (
    SignUpView,
    StudentsView,
    TeachersView,
    StuffView,
    SignUpMultyUsers,
)

urlpatterns = [
    path("sign-up/", SignUpView.as_view(), name="sign-up"),
    path("student-list/", StudentsView.as_view(), name="student-list"),
    path("teacher-list/", TeachersView.as_view(), name="teacher-list"),
    path("stuff-list/", StuffView.as_view(), name="stuff-list"),
    path("mult_sing", SignUpMultyUsers.as_view(), name="sing-mult"),
]
