from rest_framework import serializers
from django.contrib.auth.password_validation import validate_password
from uniwersity.serializer import (
    FacultytListSerializer,
    SpecialtyListSerializer,
    CourseListSerializer,
)
from user.models import UserProfile


class SignUpSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, validators=[validate_password])

    class Meta:
        model = UserProfile
        fields = ("id", "email", "password", "username", "first_name", "last_name")

    @staticmethod
    def validate_email(value):
        return value.lower()

    def create(self, validated_data):
        user = UserProfile.objects.create_user(
            username=validated_data.get("first_name"),
            first_name=validated_data.get("first_name"),
            last_name=validated_data.get("last_name"),
            email=validated_data.get("email"),
            password=validated_data.get("password"),
        )

        return user


class StudentListSerializer(serializers.ModelSerializer):
    faculty = FacultytListSerializer()
    specialty = SpecialtyListSerializer()
    course = CourseListSerializer()

    class Meta:
        model = UserProfile
        fields = ("email", "first_name", "last_name", "faculty", "specialty", "course")


class TeacherListSerializer(serializers.ModelSerializer):
    faculty = FacultytListSerializer()

    class Meta:
        model = UserProfile
        fields = ("email", "first_name", "last_name", "faculty")


class StuffListSerializer(serializers.ModelSerializer):
    faculty = FacultytListSerializer()

    class Meta:
        model = UserProfile
        fields = ("email", "first_name", "last_name", "faculty")


class ParamsSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField()
    username = serializers.CharField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()

    class Meta:
        fields = ("email", "password", "username", "first_name", "last_name")


class TestSerializer(serializers.Serializer):
    params = ParamsSerializer(many=True)
    course = serializers.IntegerField()

    class Meta:
        fields = ("params", "course")
