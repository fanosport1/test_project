from django.utils.deprecation import MiddlewareMixin
from django.utils.timezone import now
from rest_framework_simplejwt.authentication import JWTAuthentication


class LastRequestMiddleware(MiddlewareMixin):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        user = JWTAuthentication().authenticate(request)

        print(user)

        if user:
            user = user[0]
            if user.is_authenticated:
                user.last_login = now()
                user.save(update_fields=["last_login"])
        response = self.get_response(request)
        return response


class Custom(MiddlewareMixin):
    def process_request(self, request):
        breakpoint()
        pass
